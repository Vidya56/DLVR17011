# Simple Image Classification Models for the CIFAR-10 dataset using TensorFlow

This is the code for the blog post ['How to Build a Simple Image Recognition System Using TensorFlow'] with explanation that I did as an assignment for the course Deep Learning for Visual recognition.

The dataset used here is the CIFAR-10 dataset that consists of 60000 32x32 colour images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images.
It can be downloaded from https://www.cs.toronto.edu/~kriz/cifar.html.

A Softmax Classifier in tensorflow to build the image recognition system.
-
