# Assignment 2 : Simple Image Classification using Convolutional Neural Network 

This assignment is based on the blog https://becominghuman.ai/building-an-image-classifier-using-deep-learning-in-python-totally-from-a-beginners-perspective-be8dbaf22dd8.

CNN is used to build a Simple Image classification system. 
Dataset can be obtained from https://drive.google.com/drive/folders/1XaFM8BJFligrqeQdE-_5Id0V_SubJAZe
