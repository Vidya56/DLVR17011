
# coding: utf-8

# In[1]:


import sys
import os
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense, Activation
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras import callbacks


#epochs = 100

#train_data_path = './data/train'
#validation_data_path = './data/validation'


# In[2]:

from keras.models import Sequential
from keras.layers import Dense,Convolution2D,MaxPooling2D,Flatten,BatchNormalization
#from keras.layers.noise import GaussianNoise
from keras import optimizers
lr = 0.0004

classifier = Sequential()
#classifier.add(GaussianNoise(0.1,input_shape=(150,150,3)))
classifier.add(Convolution2D(input_shape=(150,150,3),filters=64,kernel_size=(3,3),activation = 'relu'))
#classifier.add(BatchNormalization())
classifier.add(MaxPooling2D(pool_size=(2,2)))

classifier.add(Convolution2D(filters=64,kernel_size=(3,3),activation = 'relu'))
#classifier.add(BatchNormalization())
classifier.add(MaxPooling2D(pool_size=(2,2)))


classifier.add(Convolution2D(filters=64,kernel_size=(3,3),activation = 'relu'))
#classifier.add(BatchNormalization())
classifier.add(MaxPooling2D(pool_size=(2,2)))

classifier.add(Convolution2D(filters=32,kernel_size=(3,3),activation = 'relu'))
#classifier.add(BatchNormalization())
classifier.add(MaxPooling2D(pool_size=(2,2)))
classifier.add(Flatten())

#classifier.add(Dense(output_dim=256,activation='relu'))
classifier.add(Dense(output_dim=128,activation='relu'))
classifier.add(Dense(output_dim=6,activation='softmax'))

classifier.compile(optimizer=optimizers.RMSprop(lr=lr),loss='categorical_crossentropy',metrics=['accuracy'])

from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory('./data/train',
                                                target_size=(150, 150),
                                                batch_size=32
                                                ,
                                                class_mode='categorical')

test_set = test_datagen.flow_from_directory('./data/validation',
                                            target_size=(150, 150),
                                            batch_size=32,
                                            class_mode='categorical')

classifier.fit_generator(training_set,
                    steps_per_epoch=20,
                    epochs=10,
                    validation_data=test_set,validation_steps=300)


# In[ ]:

target_dir = './models/'
if not os.path.exists(target_dir):
    os.mkdir(target_dir)
classifier.save('./models/new_model2.h5')
classifier.save_weights('./models/new_weights2.h5')





